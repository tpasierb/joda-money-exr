package org.joda.money.format;

public interface ExchangeRateParser {

	void parse(ExchangeRateParseContext context);

}
